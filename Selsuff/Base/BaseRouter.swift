//
//  BaseRouter.swift
//  DextionAttendance
//
//  Created by mike lim on 23/01/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import UIKit

class BaseRouter {
    
    class func createNavbar(_ root : UIViewController) -> UINavigationController {
        let navbarFrame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 64)
        let navbar = UINavigationController(rootViewController: root)
//        navbar.set(titleColor: .white, barButtonColor: .white, backgroundColor: BaseColor.primaryGradient(navbarFrame, gradientStyle: .leftToRight))
        navbar.navigationBar.isTranslucent = false
        
        return navbar
    }
    
}
