//
//  File.swift
//  Attendance
//
//  Created by mike lim on 01/07/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

public struct Margin {
    //Basic
    public static let s4: CGFloat = 4.0
    public static let s8: CGFloat = 8.0
    public static let s12:CGFloat = 12.0
    public static let s16:CGFloat = 16.0
    public static let s24:CGFloat = 24.0
    public static let s32:CGFloat = 32.0
    public static let s40:CGFloat = 40.0
    public static let s44:CGFloat = 44.0
    public static let s48:CGFloat = 48.0
    public static let s56:CGFloat = 56.0
    public static let s64:CGFloat = 64.0
    public static let s96:CGFloat = 96.0
    
    //Button
    public static let b30: CGFloat = 30.0
    public static let b35: CGFloat = 35.0
    public static let b40: CGFloat = 40.0
    public static let b45: CGFloat = 45.0
    public static let b50: CGFloat = 50.0
    
    //Icon
    public static let i10: CGFloat = 10.0
    public static let i15: CGFloat = 15.0
    public static let i20: CGFloat = 20.0
    public static let i25: CGFloat = 25.0
    public static let i30: CGFloat = 30.0
    public static let i35: CGFloat = 35.0
    
    //UIImageView
    public static let m50: CGFloat = 50.0
    public static let m60: CGFloat = 60.0
    public static let m70: CGFloat = 70.0
    public static let m80: CGFloat = 80.0
    public static let m90: CGFloat = 90.0
    public static let m100: CGFloat = 100.0
    public static let m120: CGFloat = 120.0
    
    //Font
    public static let f10: CGFloat = 10.0
    public static let f12: CGFloat = 12.0
    public static let f14: CGFloat = 14.0
    public static let f16: CGFloat = 16.0
    public static let f18: CGFloat = 18.0
    public static let f20: CGFloat = 20.0
    public static let f22: CGFloat = 22.0
    
}

public func UI<Element>(_ iPhone: Element, _ iPad: Element? = nil) -> Element {
    if UIDevice.current.userInterfaceIdiom == .phone || iPad == nil {
        return iPhone
    }
    return iPad!
}
