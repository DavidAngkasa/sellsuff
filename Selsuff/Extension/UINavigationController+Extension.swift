//
//  UINavigationController+Extension.swift
//  Attendance
//
//  Created by mike lim on 25/01/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    public func set(titleColor : UIColor, barButtonColor : UIColor, backgroundColor : UIColor) {
        navigationBar.barTintColor = backgroundColor
        navigationBar.titleTextAttributes = [.foregroundColor: titleColor]
        navigationBar.tintColor = barButtonColor
    }

    func replaceCurrentViewControllerWith(viewToGo: UIViewController, viewToBack: UIViewController, animated: Bool) {
        var controllers = viewControllers
        controllers.removeLast()
        controllers.removeLast()
        controllers.append(viewToBack)
        controllers.append(viewToGo)
        setViewControllers(controllers, animated: animated)
    }
    
    func replaceTopViewController(with viewController: UIViewController, animated: Bool) {
      var vcs = viewControllers
      vcs[vcs.count - 1] = viewController
      setViewControllers(vcs, animated: animated)
    }
}
