//
//  CGSize+Extension.swift
//  Attendance
//
//  Created by mike lim on 01/07/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit
import Foundation

extension CGSize {
    
    public init(_ size: CGFloat) {
        self.init()
        width = size
        height = size
    }
    
}
