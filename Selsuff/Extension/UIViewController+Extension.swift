//
//  UIViewController+Extension.swift
//  Attendance
//
//  Created by mike lim on 19/02/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(_ title: String, message: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert)
        
        let action = UIAlertAction(
            title: "Ok",
            style: .default,
            handler: handler)
        
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlertWithCancel(_ title: String, message: String? = nil, handler: ((UIAlertAction) -> Void)? = nil) {
        let alert = UIAlertController(
        title: title,
        message: message,
        preferredStyle: .alert)
    
        let action = UIAlertAction(
        title: NSLocalizedString("yes", comment: ""),
        style: .default,
        handler: handler)
    
        let cancel = UIAlertAction(
        title: "Cancel",
        style: .cancel,
        handler: nil)
    
        alert.addAction(action)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showLoading(loading: LoadingView) {
        loading.show(view: self.view)
    }
    
    func dismissLoading(loading: LoadingView) {
        loading.dismiss()
    }
    
    public func showNavBarLine(_ bool: Bool) {
        navigationController?.navigationBar.shadowImage = bool ? nil : UIImage()
        navigationController?.navigationBar.setBackgroundImage(bool ? nil : UIImage(), for: .default)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
