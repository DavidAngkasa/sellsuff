//
//  UICollectionView+Extension.swift
//  Attendance
//
//  Created by mike lim on 02/07/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UICollectionView {
    public func setupNormalLayout(
        itemSize: CGSize,
        verticalSpacing vSpacing: CGFloat,
        horizontalSpacing hSpacing: CGFloat,
        insets: UIEdgeInsets,
        collectionViewLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()) {
        
        // Horizontal Flow
        let layout = collectionViewLayout
        layout.scrollDirection = .horizontal
        layout.sectionInset = insets
        layout.minimumLineSpacing = vSpacing
        layout.minimumInteritemSpacing = hSpacing
        layout.itemSize = itemSize
        setCollectionViewLayout(layout, animated: true)
    }
    
    public func setupNormalLayout(
        numberOfItemsInRow totalItemsAtRow: CGFloat,
        itemHeightRatio heightRatio: CGFloat,
        verticalSpacing vSpacing: CGFloat,
        horizontalSpacing hSpacing: CGFloat,
        collectionViewWidth _width: CGFloat? = nil,
        insets: UIEdgeInsets = UIEdgeInsets(0),
        collectionViewLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()) {
        
        // Vertical Flow
        let layout = collectionViewLayout
        layout.sectionInset = insets
        layout.minimumLineSpacing = vSpacing
        layout.minimumInteritemSpacing = hSpacing
        
        setNeedsLayout()
        layoutIfNeeded()
        // substract the edges of collectionView
        var width = _width ?? frame.size.width - insets.left - insets.right
        // substract the spacing between two items
        width -= ((totalItemsAtRow - 1) * hSpacing)
        // divide the width by total items in a row
        width /= totalItemsAtRow
        let height = (width * heightRatio)
        
        layout.itemSize = CGSize(
            width: width.rounded(.down),
            height: height
        )
        setCollectionViewLayout(layout, animated: true)
    }
}
