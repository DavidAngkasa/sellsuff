//
//  UIScrollView+Extension.swift
//  Attendance
//
//  Created by mike lim on 30/07/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UIScrollView {
    public func isReachBottomView(_ offset: CGFloat = 0) -> Bool {
        setNeedsLayout()
        layoutIfNeeded()
        guard self.contentSize.height > frame.size.height else {
            return false
        }
        let currentOffset = self.contentOffset.y
        let maximumOffset = self.contentSize.height - frame.size.height + offset
        return (currentOffset >= maximumOffset) && (maximumOffset > 0) && (currentOffset > 0)
    }
    
    public func isReachBottomView(offset: CGFloat = 0, hasData: Bool, requesting: Bool) -> Bool {
        return isReachBottomView(offset) && hasData && !requesting
    }
}
