//
//  LoadingView.swift
//  Attendance
//
//  Created by mike lim on 23/02/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import Foundation
import MBProgressHUD

class LoadingView {
    
    var progressHUD = MBProgressHUD()
    
    func show(view: UIView) {
        self.progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        self.progressHUD.mode = MBProgressHUDMode.indeterminate
        self.progressHUD.label.text = "Loading"
    }
    
    func showToast(view: UIView, message: String) {
        self.progressHUD = MBProgressHUD.showAdded(to: view, animated: true)
        self.progressHUD.mode = MBProgressHUDMode.text
        self.progressHUD.label.text = message
        self.progressHUD.hide(animated: true, afterDelay: 1.5)
    }
    
    func dismiss() {
        self.progressHUD.hide(animated: true)
    }
    
}
