//
//  UITableview+Extension.swift
//  Attendance
//
//  Created by mike lim on 19/02/19.
//  Copyright © 2019 mike lim. All rights reserved.
//

import UIKit

extension UITableView {
    
    func flexibleHeight() {
        rowHeight = UITableView.automaticDimension
        estimatedRowHeight = 44
    }
    
    func setupRefreshControl(viewcontroller: UIViewController, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(viewcontroller, action: selector, for: .valueChanged)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        
        if #available(iOS 10.0, *) {
            self.refreshControl = refreshControl
        } else {
            self.addSubview(refreshControl)
        }
    }
    
    func scrollToBottom(withAnimation: Bool){
        DispatchQueue.main.async {
            if self.numberOfSections != 0 {
                let indexPath = IndexPath(
                    row: self.numberOfRows(inSection:  self.numberOfSections - 1) - 1,
                    section: self.numberOfSections - 1)
                self.scrollToRow(at: indexPath, at: .bottom, animated: withAnimation)
            }
        }
    }
    
    func dismissRefreshControl() {
        if #available(iOS 10.0, *) {
            if self.refreshControl != nil {
                self.refreshControl!.endRefreshing()
            }
        } else {
            for view in self.subviews {
                if view is UIRefreshControl {
                   (view as! UIRefreshControl).endRefreshing()
                }
            }
        }
    }
    
    func showFooterLoading() {
        guard let view = viewWithTag(29144) else {
            let activity = UIActivityIndicatorView()
            [activity].style {
                $0.startAnimating()
                $0.color = UIColor.white
                $0.tag = 29144
            }
            tableFooterView = activity
            tableFooterView?.frame.size.height = 44
            return
        }
        view.alpha = 0
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let `self` = self else { return }
            view.alpha = 1
            self.tableFooterView?.frame.size.height = 44
        }
    }
    
    func hideFooterLoading() {
        guard let view = viewWithTag(29144) else { return }
        UIView.animate(withDuration: 0.3) { [weak self] in
            guard let `self` = self else { return }
            view.alpha = 0
            self.tableFooterView?.frame.size.height = 0
        }
    }
    
    func register(_ cells: [String : AnyClass?]) {
        for (key, value) in cells {
            if let value = value {
                register(value, forCellReuseIdentifier: key)
            }
        }
    }
    
    func endlessLoad(scrollView: UIScrollView, currentTotalItem: Int, totalItem: Int, handler: () -> Void) {
        let currentOffset = scrollView.contentOffset.y.rounded()
        let maximumOffset = (scrollView.contentSize.height - scrollView.frame.size.height).rounded()
        
        if (maximumOffset == currentOffset && currentTotalItem < totalItem) {
            handler()
        }
    }
    
    func reloadData(with animation: UITableView.RowAnimation) {
        reloadSections(IndexSet(integersIn: 0..<numberOfSections), with: animation)
    }

}
